﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public enum FirstServiceLength
    {
        Single = 100000000,
        Double = 100000001,
        Triple = 100000002
    }
}
