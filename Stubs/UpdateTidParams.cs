﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    [Flags]
    public enum UpdateTidParams
    {
        OpdaterIkkeNullVaerdier = 0,
        UpdateClientId = 1,
        OpdaterRealiseretYdelseId = 2,
        OpdaterTidsafsnitId = 4,
        UpdateCourseId = 8,
        OpdaterKlippekortId = 16,
        OpdaterAfmeldingGrund = 32
    }
}
