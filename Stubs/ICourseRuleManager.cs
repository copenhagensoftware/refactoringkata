﻿using System;

namespace Kata.Stubs
{
    public interface ICourseRuleManager
    {
        CourseRule GetCourseRuleByCrmId(Guid guid);
    }
}