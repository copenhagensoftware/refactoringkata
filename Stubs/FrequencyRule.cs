﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class FrequencyRule
    {
        protected static IManagerCollection ManagerCollection
        {
            get { return null; }
        }

        private Warning _advarsel;
        private Service _ydelse;
        public long FrequencyRegelId { get; set; }
        public Guid FrequencyRegelCrmId { get; set; }
        public Guid? YdelseCrmId { get; set; }
        public int YdelseFrequency { get; set; }
        public bool RegelInaktiv { get; set; }
        public string RegelBeskrivelse { get; set; }
        public DateTime RegelGyldigFraDato { get; set; }
        public DateTime? RegelGyldigTilDato { get; set; }

        public Guid? RegelAdvarselCrmId { get; set; }

        public Warning RegelAdvarsel
        {
            get
            {
                if (_advarsel == null && RegelAdvarselCrmId.HasValue)
                {
                    // get this property the first time it is referenced

                    _advarsel = ManagerCollection.WarningManager.GetWarningByCrmId(RegelAdvarselCrmId.Value);
                }

                return _advarsel;
            }
            set { _advarsel = value; }
        }

        public Service Ydelse
        {
            get
            {
                if (_ydelse == null && YdelseCrmId.HasValue)
                {
                    // get this property the first time it is referenced

                    _ydelse = ManagerCollection.ServiceManager.GetServiceById(null, YdelseCrmId);
                }

                return _ydelse;
            }
            set { _ydelse = value; }
        }
    }
}
