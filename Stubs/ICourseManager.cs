﻿namespace Kata.Stubs
{
    public interface ICourseManager
    {
        Course GetCourseById(long v);
    }
}