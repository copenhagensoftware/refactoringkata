﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class FirstServiceInCourseRule
    {
        public long FirstServiceInCourseId { get; set; }
        public Guid FirstServiceInCourseCrmId { get; set; }
        public string FoersteYdelseIForloebNavn { get; set; }
        public Guid FoersteYdelseIForloebForloebRegelCrmId { get; set; }
        public Guid FirstServiceInCourseFirstServiceCrmId { get; set; }
        public int FirstServiceInCourseFirstTime { get; set; }
        public bool FoersteYdelseIForloebInaktiv { get; set; }
    }
}
