﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class FirstNumberOfBookingsForServiceInCourse
    {
        public int FIrstNumberOfBookingsForServiceInCourse { get; internal set; }
        public long FoersteAntalBookingerForYdelseIForloebId { get; internal set; }
        public Guid WarningCrmId { get; internal set; }
    }
}
