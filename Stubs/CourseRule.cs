﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class CourseRule
    {
        public long ForloebRegelId { get; set; }
        public Guid CourseRuleCrmId { get; set; }
        public string ForloebRegelNavn { get; set; }
        public string ForloebRegelBeskrivelse { get; set; }
        public int CourseRuleNumberOfRecommendedTreatments { get; set; }
        public int ForloebRegelLukEfterXAntalMaaneder { get; set; }
        public bool CanTherapistsAndCompanyAdminCreate { get; set; }
        public bool CanAdvisoryCenterCreate { get; set; }
        public bool CanClientCreate { get; set; }
        public bool CanClientOnlyBookFirstTime { get; set; }
        public bool CourseRuleInactive { get; set; }
        public QuestionnaireRequirementLevel? SporgeskemaTilstand { get; set; }
        public Guid? UdvalgteSporgeskemaId { get; set; }

        public bool CanChangeTherapist { get; set; }
        public Guid WarningCannotChangeTherapistCrmId { get; set; }
        public Warning AdvarselKanIkkeSkiftBehander { get; set; }
    }
}
