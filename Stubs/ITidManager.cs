﻿using System;
using System.Collections.Generic;

namespace Kata.Stubs
{
    public interface ITimeSlotManager
    {
        List<TimeSlot> GetEarlierBookingsByCourseAndService(long courseId, long serviceId, DateTime tidUtcStart);
        List<TimeSlot> GetTimesByCourseId(long courseId);
        TimeSlot GetTimeById(long timeId);
        List<TimeSlot> GetAllBookingingsInCourse(long courseId);
        List<TimeSlot> GetRelatedBookings(long courseId, long oldTimeId);
        TimeSlot GetTimeslotByCourseId(long courseId);
        List<TimeSlot> GetTiderByLokaleYdelseStatusAndTimeInterval(int status, long serviceId, long roomId, DateTime dateTime, DateTime end);
        List<TimeSlot> GetPreviousBookingsInCourse(long v, DateTime utcStart);
        long UpdateTime(long timeslotId, long timeSequenceNr, DateTime? start, DateTime? end, long? roomId, long? therapistId, long? replacementTherapistId, long? cancelledTherapistId, long? serviceId, long? realisedServiceId, long? clientId, TimeStatus? timeStatus, long? timeSectionId, Guid? coverageCrmId, Guid? productCrmId, Guid? healthAgreementCrmId, Guid? cancellationRuleCrmId, Guid? absenceRuleGuid, long? klippekortId, long? courseId, int? numberOfParticipants, string previaMembershipNumber, long? afmeldingGrund, long? parentTidId, UpdateTidParams updateTidParams);
    }
}