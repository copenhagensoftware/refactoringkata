﻿using System;

namespace Kata.Stubs
{
    public interface IWarningManager
    {
        Warning GetWarningByCrmId(Guid value);
    }
}