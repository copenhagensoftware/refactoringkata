﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public enum QuestionnaireRequirementLevel
    {
        NotRequired = 1,
        Optional = 2,
        Required = 3
    }
}
