﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public enum RuleType
    {
        VolumeRule,
        CancellationRule,
        SequenceNrMismatch,
        AbsenceRule,
        KlippekortRegel,
        CourseRule,
        TeamRule,
        ExpiryDateRule,
        TimeRule,
        FrequencyRule
    }
}
