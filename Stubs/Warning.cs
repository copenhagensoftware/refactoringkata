﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class Warning
    {
        public long WarningId { get; set; }
        public Guid WarningCrmId { get; set; }
        public string WarningText { get; set; }
        public string Advarselsbehandlertekst { get; set; }
        public string Advarselsrcbrugertekst { get; set; }
    }
}
