﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class FalckSystemException : FalckException
    {
        public FalckSystemException(string message) : base(message) { }
        public FalckSystemException(string message, Exception e) : base(message, e) { }
        public FalckSystemException(string message, params object[] args) : base(string.Format(message, args)) { }
        public FalckSystemException(Exception e, string message, params object[] args) : base(string.Format(message, args), e) { }
    }
}
