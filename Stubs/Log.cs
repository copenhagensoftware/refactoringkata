﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    class Log
    {
        public static void Debug(string className, string message, params object[] args) { }
        public static void Error(string className, string message, params object[] args) { }
        public static void Info(string className, string message, params object[] args) { }
        internal static void Warn(string className, string message, params object[] args) { }
    }
}
