﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public enum Scenario : int
    {
        Aflysning = 0,
        Afmelding = 1,
        Booking = 2,
        Moving = 3,
        Reminder = 4,
        AnnulleringVedAendringAfKlippekortsregel = 5,
        AnnulleringVedAendringAfVolumeregel = 6,
        ManglerAfloeser = 7,
        BehandlerSkifte = 8,
        AflysningPgaFravaer = 9,
        AfmeldtPgaFoersteTidIForloebAfmeldt = 10,
        AflystPgaUdeblivelseFraFoersteTidIForloeb = 11,
        FjernPaamindelserBatchJob = 12,
        MangeAfloeserGodkendelses = 13,
        MangeAfloeserAfsla = 14,
    }
}
