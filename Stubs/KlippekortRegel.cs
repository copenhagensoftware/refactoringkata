﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class KlippekortRegel
    {
        public long KlippekortRegelId { get; set; }
        public Guid KlippekortRegelCrmId { get; set; }
        public Guid? KlippekortRegelSundhedsCrmId { get; set; }
        public Guid? KlippekortRegelProduktCrmId { get; set; }
        public Guid? KlippekortRegelDaekningsCrmId { get; set; }
        public Guid? KlippekortRegelYdelseCrmId { get; set; }
        public Guid? KlippekortRegelProduktGruppeCrmId { get; set; }
        public int KlippekortRegelType { get; set; }
        public int KlippekortRegelGyldighed { get; set; }
        public DateTime? KlippekortRegelStartdato { get; set; }
        public int KlippekortRegelAntalKlip { get; set; }
        public decimal? KlippekortRegelAntalKr { get; set; }
        public bool KlippekortRegelFornyelseVedUdloebet { get; set; }
        public bool KlippekortRegelAktiveresVedFoersteBooking { get; set; }
        public Guid KlippekortRegelAdvarselIkkeFlereKlippekortIOrdningenCrmId { get; set; }
        public Guid KlippekortRegelAdvarselIkkeFlereKlipTilbageCrmId { get; set; }
        public DateTime KlippekortRegelGyldigFra { get; set; }
        public DateTime KlippekortRegelGyldigTil { get; set; }
        public bool KlippekortRegelInaktiv { get; set; }

        public Warning KlippekortRegelAdvarselIkkeFlereKlipTilbage { get; set; }
        public Warning KlippekortRegelAdvarselIkkeFlereKlippekortIOrdningen { get; set; }
    }
}
