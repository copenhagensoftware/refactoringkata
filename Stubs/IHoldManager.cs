﻿namespace Kata.Stubs
{
    public interface IHoldManager
    {
        void CancelTeamTimeslot(long tidId, long v, string previaMedlemsNummer);
    }
}