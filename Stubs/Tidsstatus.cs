﻿namespace Kata.Stubs
{
    public enum TimeStatus
    {
        Free = 1,
        FreeEmergency = 2,
        BookedForTreatment = 3,
        BookedForMeeting = 4,
        BookedRoomOccupied = 5,
        Pause = 6,
        Reserved = 7,
        Blocked = 8,
        AflystAfBehandlerGrundetSygdom = 9,
        AflystAfBehandlerGrundetAndet = 10,
        AfmeldtAfKlient = 11,
        UdeblivelseAfBehandler = 12,
        UdeblivelseAfKlient = 13,
        ForSentAflystAfBehandler = 14,
        ForSentAfmeldtAfKlient = 15,
        AnnulleringVedAendringAfKlippekortsregel = 16,
        AnnulleringGrundetDeaktivering = 17,
        AnnulleringVedAendringAfVolumeregel = 18,
        AflystGrundetAendringAfTidsafsnit = 19,
        AdminMoede = 20
    }
}
