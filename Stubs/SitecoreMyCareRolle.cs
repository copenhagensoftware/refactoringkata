﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public enum SitecoreMyCareRole
    {
        Therapist,
        Planlægning,
        Regionsleder,
        CompanyAdmin,
        Advisor,
        Client,
        ExternalBooker,
        Unknown,
    }
}
