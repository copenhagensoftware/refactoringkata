﻿using System;

namespace Kata.Stubs
{
    public interface IFirstNumberOfBookingsForServiceInCourseManager
    {
        FirstNumberOfBookingsForServiceInCourse GetFirstNumberOfBookingsForServiceInCourseByCourseCrmIdAndServiceCrmId(
            Guid courseRuleCrmId, Guid serviceCrmId, bool v);
    }
}