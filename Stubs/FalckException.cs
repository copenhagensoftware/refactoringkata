﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    [Serializable]
    public abstract class FalckException : Exception
    {
        protected FalckException(string message, Exception e) : base(message, e) { }
        protected FalckException(Exception e) : base(string.Format("Der opstod en ukendt fejl: {{Type: '{0}'}}", e.GetType().FullName), e) { }
        protected FalckException(string message) : base(message) { }
        protected FalckException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
