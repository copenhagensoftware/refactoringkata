﻿using Kata.Stubs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kata
{
    public class CourseRules
    {
        private static readonly string ClassName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        private static IManagerCollection ManagerCollection
        {
            get { return null; }
        }

        #region Private Methods

        private int CheckFirstNumberOfBookingsForServiceInCourseRules(Guid courseRuleCrmId, Service service, long courseId, DateTime timeUtcStart, int numberOfSlotsToBook)
        {
            FirstNumberOfBookingsForServiceInCourse firstNumberOfBookings;
            var doesServiceMatch = DoesServiceMatchFirstNumberOfBookingsForServiceInCourseRule(courseRuleCrmId, service.ServiceCrmId, out firstNumberOfBookings);
            if (!doesServiceMatch)
            {
                return numberOfSlotsToBook;
            }

            var isFirstBookingForServiceInCourse = IsFirstBookingForServiceInCourse(courseId, service.ServiceId, timeUtcStart);
            if (!isFirstBookingForServiceInCourse)
            {
                return numberOfSlotsToBook;
            }

            numberOfSlotsToBook = HowManyTreatmentsMustBeBookedFirstTimeForServiceInCourse(firstNumberOfBookings);

            return numberOfSlotsToBook;
        }

        private bool DoesServiceMatchFirstNumberOfBookingsForServiceInCourseRule(Guid courseRuleCrmId, Guid serviceCrmId, out FirstNumberOfBookingsForServiceInCourse firstNumberOfBookings)
        {
            firstNumberOfBookings = ManagerCollection.FirstNumberOfBookingsForServiceInCourseManager.GetFirstNumberOfBookingsForServiceInCourseByCourseCrmIdAndServiceCrmId(courseRuleCrmId, serviceCrmId, false);
            if (firstNumberOfBookings == null)
            {
                return false;
            }
            return true;
        }

        private bool IsFirstBookingForServiceInCourse(long forloebId, long ydelseId, DateTime tidUtcStart)
        {
            var earlierBookings = ManagerCollection.TimeManager.GetEarlierBookingsByCourseAndService(forloebId, ydelseId, tidUtcStart);
            if (0 < earlierBookings.Count)
            {
                return false;
            }
            return true;
        }

        private int HowManyTreatmentsMustBeBookedFirstTimeForServiceInCourse(FirstNumberOfBookingsForServiceInCourse rule)
        {
            int? numberOfSlotsToBook = null;
            if (rule.FIrstNumberOfBookingsForServiceInCourse == (int)FirstServiceLength.Single)
            {
                numberOfSlotsToBook = 1;
            }
            if (rule.FIrstNumberOfBookingsForServiceInCourse == (int)FirstServiceLength.Double)
            {
                numberOfSlotsToBook = 2;
            }
            if (rule.FIrstNumberOfBookingsForServiceInCourse == (int)FirstServiceLength.Triple)
            {
                numberOfSlotsToBook = 3;
            }
            if (numberOfSlotsToBook != null)
            {
                return numberOfSlotsToBook.GetValueOrDefault();
            }

            var err = String.Format("Ukendt antal bookinger '{0}'. FoersteAntalBookingerForYdelseIForloebRegelId '{1}'", rule.FIrstNumberOfBookingsForServiceInCourse, rule.FoersteAntalBookingerForYdelseIForloebId);
            throw new FalckSystemException(err);
        }

        private int HowManyTreatmentsAreToBeBookedFirstTime(CourseRule courseRule, FirstServiceInCourseRule firstServiceInCourseRule, Guid serviceCrmId)
        {
            if (firstServiceInCourseRule == null)
            {
                List<FirstServiceInCourseRule> firstServiceInCourseRules = ManagerCollection.FirstServiceInCourseManager.GetFirstServiceForCourseRule(courseRule.CourseRuleCrmId);
                if (firstServiceInCourseRules.Count == 0)
                {
                    return 1;
                }

                foreach (var firstServiceRule in firstServiceInCourseRules)
                {
                    if (firstServiceRule.FirstServiceInCourseCrmId == serviceCrmId)
                    {
                        firstServiceInCourseRule = firstServiceRule;
                    }
                }

                if (firstServiceInCourseRule == null)
                {
                    var msg = String.Format("Kunne ikke finde et match mellem ydelsen og første ydelser på forløbet. Kan ikke bestemme antal behandlinger. ForloebRegelId '{0}', ydelseCrmId '{1}'", courseRule.ForloebRegelId, serviceCrmId);
                    throw new FalckSystemException(msg);
                }
            }

            if (firstServiceInCourseRule.FirstServiceInCourseFirstTime == (int)FirstServiceLength.Single)
            {
                return 1;
            }

            if (firstServiceInCourseRule.FirstServiceInCourseFirstTime == (int)FirstServiceLength.Double)
            {
                return 2;
            }
            if (firstServiceInCourseRule.FirstServiceInCourseFirstTime == (int)FirstServiceLength.Triple)
            {
                return 3;
            }

            var err = String.Format("Ukendt antal ydelser '{0}'. FoersteYdelseIForloebRegelId '{1}'", firstServiceInCourseRule.FirstServiceInCourseFirstTime, firstServiceInCourseRule.FirstServiceInCourseId);
            throw new FalckSystemException(err);
        }

        private bool IsBookingWithinMaxLimit(CourseRule courseRule, Course course, int numberOfTimeSlotsToBook, out List<BrokenRule> brokenCourseRules)
        {
            brokenCourseRules = new List<BrokenRule>();

            var max = courseRule.CourseRuleNumberOfRecommendedTreatments;
            var numberUsed = (course != null ? course.numberOfTreatmentsUsed : 0);
            var futureNumber = (numberUsed + numberOfTimeSlotsToBook);
            if (futureNumber <= max)
            {
                return true;
            }

            brokenCourseRules = new List<BrokenRule>() { new BrokenRule()
                    {
                        WarningText = TextManager.GetText("Forloeb_MaksBookingerOvertraadt", "Forløbets maksimale anbefalede antal bookinger overskrides"),
                        Blocking = false,
                        RuleType = RuleType.CourseRule
                    }
                };
            return false;
        }

        private bool CanRoleOnlyBookFirstTime(CourseRule courseRule, SitecoreMyCareRole role, out List<BrokenRule> brokenCourseRules)
        {
            brokenCourseRules = new List<BrokenRule>();

            if (role == SitecoreMyCareRole.Unknown)
            {
                const string msg = "Ukendt rolle ved tjek af Forløbsregler";
                throw new FalckSystemException(msg);
            }

            if (role != SitecoreMyCareRole.Client)
            {
                return true;
            }

            var ok = !courseRule.CanClientOnlyBookFirstTime;
            if (ok)
            {
                return true;
            }

            brokenCourseRules = new List<BrokenRule>() { new BrokenRule()
                    {
                        WarningText = TextManager.GetText("Forloeb_KlientMaaKunBookeFoersteTid", "Forløbet tillader kun, at klienten booker første tid"),
                        Blocking = true,
                        RuleType = RuleType.CourseRule
                    }
                };

            return false;
        }

        private bool DoesTimeMatchFirstRequiredService(long newTimeId, long? oldTimeId, Service timeService, Course course, CourseRule courseRule, Scenario scenario, out FirstServiceInCourseRule firstServiceInCourseRule, out List<BrokenRule> brokenCourseRules)
        {
            brokenCourseRules = new List<BrokenRule>();
            firstServiceInCourseRule = null;

            List<FirstServiceInCourseRule> firstServiceInCourseRules = ManagerCollection.FirstServiceInCourseManager.GetFirstServiceForCourseRule(courseRule.CourseRuleCrmId);
            if (firstServiceInCourseRules.Count == 0)
            {
                return true;
            }

            firstServiceInCourseRule = firstServiceInCourseRules[0];

            var warningText = string.Empty;
            var courseSlots = course != null ? ManagerCollection.TimeManager.GetTimesByCourseId(course.CourseId) : new List<TimeSlot>();
            if (course == null || courseSlots.Count == 0)
            {
                if (timeService.ServiceCrmId != firstServiceInCourseRule.FirstServiceInCourseFirstServiceCrmId)
                {
                    warningText = TextManager.GetText("Forloeb_IkkeMatchPåFørsteYdelse", "Bookingen kan ikke foretages, da forløbsreglen ikke tillader den valgte ydelse som første booking i forløbet");
                }
            }
            else
            {
                var firstSlotInCourse = courseSlots[0];
                TimeSlot newSlot = ManagerCollection.TimeManager.GetTimeById(newTimeId);
                var oldTid = oldTimeId != null ?
                    ManagerCollection.TimeManager.GetTimeById(oldTimeId.GetValueOrDefault()) :
                    newSlot;
                if (timeService.ServiceCrmId != firstServiceInCourseRule.FirstServiceInCourseFirstServiceCrmId && newSlot.UtcStart < firstSlotInCourse.UtcStart)
                {
                    warningText = scenario == Scenario.Moving ?
                        TextManager.GetText("Forloeb_IkkeMatchPåFørsteYdelseFlytning", "Flytningen kan ikke foretages, da næste tid i forløbet ikke stemmer overens med den krævede første ydelse i forløbet") :
                        TextManager.GetText("Forloeb_IkkeMatchPåFørsteYdelse", "Bookingen kan ikke foretages, da forløbsreglen ikke tillader den valgte ydelse som første booking i forløbet");
                }
                else if (oldTid.TimeSlotId == firstSlotInCourse.TimeSlotId &&
                        courseSlots.Count > 1 &&
                        courseSlots[1].UtcStart < newSlot.UtcStart)
                {
                    Service service = ManagerCollection.ServiceManager.GetServiceById(courseSlots[1].ServiceId, null);
                    if (service.ServiceCrmId != firstServiceInCourseRule.FirstServiceInCourseFirstServiceCrmId)
                    {
                        warningText = scenario == Scenario.Moving ?
                            TextManager.GetText("Forloeb_IkkeMatchPåFørsteYdelseFlytning", "Flytningen kan ikke foretages, da næste tid i forløbet ikke stemmer overens med den krævede første ydelse i forløbet") :
                            TextManager.GetText("Forloeb_IkkeMatchPåFørsteYdelse", "Bookingen kan ikke foretages, da forløbsreglen ikke tillader den valgte ydelse som første booking i forløbet");
                    }
                }
            }

            if (!string.IsNullOrEmpty(warningText))
            {
                var rule = new BrokenRule()
                {
                    WarningText = warningText,
                    Blocking = true,
                    RuleType = RuleType.CourseRule
                };

                brokenCourseRules.Add(rule);
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsNewBookingBeforeFirstBooking(DateTime timeUtcEnd, long courseId)
        {
            bool result = false;

            List<TimeSlot> timeSlots = ManagerCollection.TimeManager.GetAllBookingingsInCourse(courseId);
            var sortedTimes = timeSlots.OrderBy(t => t.StartLocalTime).ToList();
            if(sortedTimes.Count>0)
                result = sortedTimes[0].StartLocalTime > timeUtcEnd ? true : false;

            return result;
        }

        private List<BrokenRule> CheckForFirstBooking(DateTime timeUtcEnd, long courseId, List<BrokenRule> brokenCourseRules)
        {
            var IsbeforeFirstBooking = IsNewBookingBeforeFirstBooking(timeUtcEnd, courseId);
            if (IsbeforeFirstBooking)
            {
                var brokenRule = new BrokenRule()
                {
                    WarningText = "It is not possible to book a time before first booking on a    course",
                    Blocking = true,
                    RuleType = RuleType.CourseRule
                };
                brokenCourseRules.Add(brokenRule);
            }

            return brokenCourseRules;
        }

        #endregion

        #region Public Methods

        public List<BrokenRule> IsBookingLegal(Profile profile, long timeSlotId, long? oldTimeSlotId, DateTime timeUtcStart, DateTime timeUtcEnd, long roomId, long serviceId, long? courseId, SitecoreMyCareRole role, Scenario scenario, bool? movingToSameDay, bool executeRules, out int numberOfTimeSlotsToBook)
        {
            var brokenCourseRules = new List<BrokenRule>();
            var timeslotMatchesFirstRequiredService = false;

            numberOfTimeSlotsToBook = 0;

            if (profile.CourseRuleCrmId == null)
            {
                numberOfTimeSlotsToBook = 1;
                return brokenCourseRules;
            }

            if (courseId == null || courseId == -1)
            {
                numberOfTimeSlotsToBook = 1;
                return brokenCourseRules;
            }

            if (scenario == Scenario.Moving && movingToSameDay == true)
            {
                return new List<BrokenRule>();
            }

            CourseRule courseRule = ManagerCollection.CourseRuleManager.GetCourseRuleByCrmId(profile.CourseRuleCrmId.GetValueOrDefault());

            if (courseRule == null)
            {
                var msg = String.Format("Kunne ikke finde forloebRegel med id '{0}'", profile.CourseRuleCrmId.GetValueOrDefault());
                throw new FalckSystemException(msg);
            }

            if (courseRule.CourseRuleInactive)
            {
                return new List<BrokenRule>();
            }
            var isFirstBookingInCourse = false;
            Course course = null;
            if (courseId == -2) 
            {
                isFirstBookingInCourse = true;
            }
            else
            {
                course = ManagerCollection.CourseManager.GetCourseById(courseId.GetValueOrDefault());
                if (course == null)
                {
                    var msg = String.Format("Kunne ikke finde forloeb med id '{0}'", courseId);
                    throw new FalckSystemException(msg);
                }

                isFirstBookingInCourse = (course.numberOfTreatmentsUsed == 0);
            }


            var isFirstServiceInCourseRuleUsed = false;
            var service = ManagerCollection.ServiceManager.GetServiceById(serviceId, null);


            FirstServiceInCourseRule firstServiceInCourseRule = null;
            timeslotMatchesFirstRequiredService = DoesTimeMatchFirstRequiredService(timeSlotId, oldTimeSlotId, service, course, courseRule, scenario, out firstServiceInCourseRule, out brokenCourseRules);
            if (!timeslotMatchesFirstRequiredService)
            {
                return brokenCourseRules;
            }

            if (isFirstBookingInCourse)
            {

                numberOfTimeSlotsToBook = HowManyTreatmentsAreToBeBookedFirstTime(courseRule, firstServiceInCourseRule, service.ServiceCrmId);
                if (firstServiceInCourseRule != null)
                {
                    isFirstServiceInCourseRuleUsed = true;
                }
            }
            else
            {
                CheckForFirstBooking(timeUtcEnd, (long)courseId, brokenCourseRules);
                if (brokenCourseRules.Count > 0 && brokenCourseRules.Any(rules => rules.Blocking)) return brokenCourseRules;
                else
                {
                    timeslotMatchesFirstRequiredService = CanRoleOnlyBookFirstTime(courseRule, role, out brokenCourseRules);
                    if (!timeslotMatchesFirstRequiredService) { return brokenCourseRules; }

                    if (oldTimeSlotId != null)
                    {
                        List<TimeSlot> relatedTimeSlots = ManagerCollection.TimeManager.GetRelatedBookings((long)courseId, (long)oldTimeSlotId);
                        numberOfTimeSlotsToBook = relatedTimeSlots.Count;
                    }
                    else
                        numberOfTimeSlotsToBook = 1;

                }
            }

            if (isFirstBookingInCourse == false)
            {
                var timeslot = ManagerCollection.TimeManager.GetTimeById(timeSlotId);
                long? courseTherapistID = course.TherapistId;
                if (courseTherapistID == null || courseTherapistID == 0)
                {
                    TimeSlot lastTimeSlot = ManagerCollection.TimeManager.GetTimeslotByCourseId(course.CourseId);
                    courseTherapistID = lastTimeSlot.TherapistId;
                }
                else
                {
                    courseTherapistID = course.TherapistId;
                }
                if (!isFirstBookingInCourse)
                {
                    List<TimeSlot> bookings = ManagerCollection.TimeManager.GetEarlierBookingsByCourseAndService(course.CourseId, service.ServiceId, DateTime.MaxValue);
                    if (bookings.Count > 0 && bookings.Any(booking => booking.TherapistId != timeslot.TherapistId))
                    {
                        if (courseRule.CanChangeTherapist)
                        {

                            FirstNumberOfBookingsForServiceInCourse firstNumberOfBookingsForServiceInCourse = new FirstNumberOfBookingsForServiceInCourse();
                            firstNumberOfBookingsForServiceInCourse = ManagerCollection.FirstNumberOfBookingsForServiceInCourseManager.GetFirstNumberOfBookingsForServiceInCourseByCourseCrmIdAndServiceCrmId(courseRule.CourseRuleCrmId, service.ServiceCrmId, true);
                            numberOfTimeSlotsToBook = HowManyTreatmentsMustBeBookedFirstTimeForServiceInCourse(firstNumberOfBookingsForServiceInCourse);
                            var text = "";
                            Warning advarsel = ManagerCollection.WarningManager.GetWarningByCrmId(firstNumberOfBookingsForServiceInCourse.WarningCrmId);
                            if (advarsel != null && advarsel.WarningText != null)
                            {
                                text = advarsel.WarningText;
                            }
                            var brudtRegel = new BrokenRule()
                            {
                                WarningText = text,
                                Blocking = false,
                                RuleType = RuleType.CourseRule
                            };
                            brokenCourseRules.Add(brudtRegel);
                            return brokenCourseRules;
                        }

                        else
                        {
                            var text = "";
                            Warning warning = ManagerCollection.WarningManager.GetWarningByCrmId(courseRule.WarningCannotChangeTherapistCrmId);
                            if (warning != null && warning.WarningText != null)
                            {
                                text = warning.WarningText;
                            }
                            var brokenRule = new BrokenRule()
                            {
                                WarningText = text,
                                Blocking = true,
                                RuleType = RuleType.CourseRule
                            };
                            brokenCourseRules.Add(brokenRule);
                            return brokenCourseRules;
                        }
                    }

                    else
                    {
                        numberOfTimeSlotsToBook = CheckFirstNumberOfBookingsForServiceInCourseRules(courseRule.CourseRuleCrmId, service, courseId.GetValueOrDefault(), timeUtcStart, numberOfTimeSlotsToBook);

                    }

                }
                else if (!isFirstServiceInCourseRuleUsed)
                {
                    numberOfTimeSlotsToBook = CheckFirstNumberOfBookingsForServiceInCourseRules(courseRule.CourseRuleCrmId, service, courseId.GetValueOrDefault(), timeUtcStart, numberOfTimeSlotsToBook);
                }
            }

            timeslotMatchesFirstRequiredService = IsBookingWithinMaxLimit(courseRule, course, numberOfTimeSlotsToBook, out brokenCourseRules);

            if (1 < numberOfTimeSlotsToBook)
            {
                var end = timeUtcEnd.ToLocalTime().AddDays(1);

                var status = TimeStatus.Free;
                List<TimeSlot> timeslots = ManagerCollection.TimeManager.GetTiderByLokaleYdelseStatusAndTimeInterval((int)status, serviceId, roomId, timeUtcEnd.ToUniversalTime(), end);

                var numberOfExtraTimeslotsToBook = numberOfTimeSlotsToBook - 1;
                if (timeslots.Count < numberOfExtraTimeslotsToBook)
                {
                    var msg = String.Format(
                           TextManager.GetText("Forloeb_KanIkkeFindeEktraTiderTilDobbeltTrippeltRid",
                           "Kan ikke finde {0} ekstra ledige tider til dobbelt/trippelt tid"),
                        numberOfExtraTimeslotsToBook);
                    var brokenRule = new BrokenRule()
                    {
                        WarningText = msg,
                        Blocking = true,
                        RuleType = RuleType.CourseRule
                    };
                    brokenCourseRules.Add(brokenRule);
                }

                else
                {
                    var lastEnd = timeUtcEnd.ToUniversalTime();
                    var nextStart = timeslots[0].UtcStart.ToUniversalTime();
                    if (!lastEnd.Equals(nextStart))
                    {

                        var msg = String.Format(
                           TextManager.GetText("Forloeb_KanIkkeFindeLedigTidSomStarterHvorFoersteTidSlutter",
                           "Kan ikke finde en ledig tid, som starter {0} hvor den første tid slutter"),
                           lastEnd.ToLocalTime().ToString("dd-MM-yyyy hh:mm"));
                        var brudtRegel = new BrokenRule()
                        {
                            WarningText = msg,
                            Blocking = true,
                            RuleType = RuleType.CourseRule
                        };
                        brokenCourseRules.Add(brudtRegel);
                        return brokenCourseRules;
                    }


                    if (numberOfTimeSlotsToBook == 3)
                    {
                        var newEnd = timeslots[0].UtcEnd.ToUniversalTime();

                        if (numberOfTimeSlotsToBook >= 3 && !timeslots[1].UtcStart.ToUniversalTime().Equals(newEnd))
                        {
                            var msg = String.Format(
                                TextManager.GetText("Forloeb_KanIkkeFindeLedigTidSomStarterHvorAndenTidSlutter",
                                "Kan ikke finde en ledig tid, som starter {0} hvor den anden tid slutter"),
                                newEnd.ToLocalTime().ToString("dd-MM-yyyy hh:mm"));
                            var brokenRule = new BrokenRule()
                            {
                                WarningText = msg,
                                Blocking = true,
                                RuleType = RuleType.CourseRule
                            };
                            brokenCourseRules.Add(brokenRule);
                            return brokenCourseRules;
                        }

                    }
                }
            }

            return brokenCourseRules;
        }

        public List<BrokenRule> CheckCourseRuleOnAbsenceAndCancellation(TimeSlot timeslot, Scenario scenario, bool? movingToSameDay)
        {
            var brokenCourseRule = new List<BrokenRule>();

            if (timeslot == null)
            {
                var msg = String.Format(ClassName, "TjekForloebsreglerVedAfmeldingOgUdeblivelse. Tid er null");
                throw new FalckSystemException(msg);
            }

            if (timeslot.CourseId == null || timeslot.CourseId == -1 || timeslot.CourseId.GetValueOrDefault() == 0)
            {
                return new List<BrokenRule>();
            }

            if (scenario == Scenario.Moving && movingToSameDay == true)
            {
                return new List<BrokenRule>();
            }

            var timeslotsCourse = ManagerCollection.CourseManager.GetCourseById(timeslot.CourseId.GetValueOrDefault());
            if (timeslotsCourse == null)
            {
                var msg = String.Format("Kunne ikke finde forloeb med id '{0}'", timeslot.CourseId.GetValueOrDefault());
                throw new FalckSystemException(msg);
            }

            var courseRule = ManagerCollection.CourseRuleManager.GetCourseRuleByCrmId(timeslotsCourse.CourseRuleId);

            if (courseRule == null)
            {
                var msg = String.Format("Kunne ikke finde forloebRegel med id '{0}'", timeslotsCourse.CourseRuleId);
                throw new FalckSystemException(msg);
            }

            if (courseRule.CourseRuleInactive)
            {
                return new List<BrokenRule>();
            }

            var service = ManagerCollection.ServiceManager.GetServiceById(timeslot.ServiceId, null);
            var firstServiceInCourseRules = ManagerCollection.FirstServiceInCourseManager.GetFirstServiceForCourseRule(courseRule.CourseRuleCrmId);
            var usedFirstService = firstServiceInCourseRules.FirstOrDefault(r => r.FirstServiceInCourseFirstServiceCrmId == service.ServiceCrmId);

            if (usedFirstService == null)
            {
                return brokenCourseRule;
            }

            List<TimeSlot> previousBookingsInCourse = ManagerCollection.TimeManager.GetPreviousBookingsInCourse(timeslot.CourseId.GetValueOrDefault(), timeslot.UtcStart);
            var numberOfFirstBookings = GetFirstNumberOfBookings(usedFirstService);

            if (previousBookingsInCourse.Count < numberOfFirstBookings)
            {
                brokenCourseRule = new List<BrokenRule>(){
                    new BrokenRule()
                {
                    WarningText = TextManager.GetText("Forloeb_Afmeld_UdeblivFraFoersteBooking", "Du er ved at afmelde/udeblive fra den første booking i forløbet. Dette betyder" +
                                                                              " at alle fremtidige bookinger på forløbet vil blive slettet"),
                    DeleteFutureBookingsInCourse = true,
                    RuleType = RuleType.CourseRule
                }};
            }
            return brokenCourseRule;
        }

        public long GetFirstNumberOfBookings(FirstServiceInCourseRule rule)
        {
            if (rule.FirstServiceInCourseFirstTime == (int)FirstServiceLength.Single)
            {
                return 1;
            }

            if (rule.FirstServiceInCourseFirstTime == (int)FirstServiceLength.Double)
            {
                return 2;
            }
            if (rule.FirstServiceInCourseFirstTime == (int)FirstServiceLength.Triple)
            {
                return 3;
            }
            else
            {
                var msg = String.Format("Ukendt antal foerse ydelser i forloeb '{0}'", rule.FirstServiceInCourseFirstTime);
                throw new FalckSystemException(msg);
            }
        }

        public void CancelBooking(TimeSlot time, string previaMembershipNumber, long? clientBeingCancelledId)
        {
            var newStatus = TimeStatus.Free;
            if (time.IsEmergency.GetValueOrDefault()) { newStatus = TimeStatus.FreeEmergency; }

            if (time.Team)
            {
                ManagerCollection.TeamManager.CancelTeamTimeslot(time.TimeSlotId, clientBeingCancelledId.GetValueOrDefault(), previaMembershipNumber);
                time.NumberOfParticipants--;
            }
            ManagerCollection.TimeManager.UpdateTime(time.TimeSlotId, time.SequenceNumber, null, null, null, null, null, null, null, null, null, newStatus,
                null, Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty, Guid.Empty, null, null, time.NumberOfParticipants, previaMembershipNumber, null, null,
                UpdateTidParams.UpdateClientId | UpdateTidParams.UpdateCourseId | UpdateTidParams.OpdaterKlippekortId);
        }

        public List<BrokenRule> CheckChangeTherapistRules(Course course, Service service, long therapistId)
        {
            var brokenCourseRules = new List<BrokenRule>();
            var text = "";

            List<TimeSlot> bookings = ManagerCollection.TimeManager.GetEarlierBookingsByCourseAndService(course.CourseId, service.ServiceId, DateTime.Now);

            if (bookings.Count > 0 && bookings.Any(booking => booking.TherapistId != therapistId))
            {
                
                FirstNumberOfBookingsForServiceInCourse firstNumberOfBookingsForServiceInCourse = new FirstNumberOfBookingsForServiceInCourse();
                firstNumberOfBookingsForServiceInCourse = ManagerCollection.FirstNumberOfBookingsForServiceInCourseManager.GetFirstNumberOfBookingsForServiceInCourseByCourseCrmIdAndServiceCrmId(course.CourseRuleId, service.ServiceCrmId, true);
                if (firstNumberOfBookingsForServiceInCourse != null)
                {
                    Warning warning = ManagerCollection.WarningManager.GetWarningByCrmId(firstNumberOfBookingsForServiceInCourse.WarningCrmId);
                    if (warning != null && warning.WarningText != null)
                    {
                        text = warning.WarningText;
                    }
                    var brokenRule =
                            new BrokenRule
                            {
                                RuleType = RuleType.CourseRule,
                                WarningText = text.ToString(),
                                Blocking = false,

                            };
                    brokenCourseRules.Add(brokenRule);
                }

                return brokenCourseRules;

            }
        
            else
            {
                return new List<BrokenRule>();
            }

        }


        public bool CanRoleCreateCourse(CourseRule courseRule, SitecoreMyCareRole role, out List<BrokenRule> brokenCourseRules)
        {
            brokenCourseRules = new List<BrokenRule>();

            var result = false;
            if (role == SitecoreMyCareRole.Therapist || role == SitecoreMyCareRole.CompanyAdmin)
            {
                result = courseRule.CanTherapistsAndCompanyAdminCreate;
            }
            if (role == SitecoreMyCareRole.Client)
            {
                result = courseRule.CanClientCreate;
            }
            if (role == SitecoreMyCareRole.Advisor)
            {
                result = courseRule.CanAdvisoryCenterCreate;
            }

            if(role==SitecoreMyCareRole.ExternalBooker)
            {
                result = true;
            }

            if (!result)
            {
                brokenCourseRules.Add(new BrokenRule()
                {
                    WarningText = String.Format(
                            TextManager.GetText("Forloeb_OpretIkkeTilladtForRolle", "Rollen {0} må ikke oprette forløb"),
                            role),
                    Blocking = true,
                    RuleType = RuleType.CourseRule
                }
                );
            }

            return result;
        }

        #endregion
    }
}
