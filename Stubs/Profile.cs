﻿using System;

namespace Kata.Stubs
{
    public class Profile
    {
        protected static IManagerCollection ManagerCollection
        {
            get { return null; }
        }

        private KlippekortRegel _sundhedsKlippekortRegel;
        private KlippekortRegel _produktKlippekortRegel;
        private KlippekortRegel _daekningKlippekortRegel;
        private CourseRule _forloebRegel;

        //Added by RME for frequency
        private FrequencyRule _frequencyRegel;
        public long ProfilId { get; set; }
        public Guid GruppeCrmId { get; set; }
        public Guid SundhedsordningCrmId { get; set; }
        public Guid ProduktCrmId { get; set; }
        public Guid? DaekningCrmId { get; set; }
        public Guid ProduktGruppeCrmId { get; set; }
        public Guid YdelseCrmId { get; set; }
        public int YdelseVarighedMinutter { get; set; }
        public Guid KundeCrmId { get; set; }
        public Guid? CourseRuleCrmId { get; set; }
        public Guid? SundhedsKlippekortRegelCrmId { get; set; }
        public int ProfilSLADays { get; set; }
        public double ProfilSLADistance { get; set; }
        public CourseRule ForloebRegel
        {
            get
            {
                if (_forloebRegel == null && CourseRuleCrmId.HasValue)
                {
                    // get this property the first time it is referenced

                    _forloebRegel =
                        ManagerCollection.CourseRuleManager.GetCourseRuleByCrmId(CourseRuleCrmId.Value);
                }

                return _forloebRegel;
            }
            set { _forloebRegel = value; }
        }
       

        public KlippekortRegel SundhedsKlippekortRegel
        {
            get
            {
                if (_sundhedsKlippekortRegel == null && SundhedsKlippekortRegelCrmId.HasValue)
                {
                    // get this property the first time it is referenced
                    _sundhedsKlippekortRegel = ManagerCollection.KlippekortRegelManager.GetKlippekortRegelByCrmId(SundhedsKlippekortRegelCrmId.Value);
                }


                return _sundhedsKlippekortRegel;
            }
            set { _sundhedsKlippekortRegel = value; }
        }



        public Guid? ProduktKlippekortRegelCrmId { get; set; }

        public KlippekortRegel ProduktKlippekortRegel
        {
            get
            {
                if (_produktKlippekortRegel == null && ProduktKlippekortRegelCrmId.HasValue)
                {
                    // get this property the first time it is referenced
                    _produktKlippekortRegel = ManagerCollection.KlippekortRegelManager.GetKlippekortRegelByCrmId(ProduktKlippekortRegelCrmId.Value);
                }


                return _produktKlippekortRegel;
            }
            set { _produktKlippekortRegel = value; }
        }

        public Guid? DaekningKlippekortRegelCrmId { get; set; }

        public KlippekortRegel DaekningKlippekortRegel
        {
            get
            {
                if (_daekningKlippekortRegel == null && DaekningAfmeldingsRegelCrmId.HasValue)
                {
                    // get this property the first time it is referenced
                    _daekningKlippekortRegel = ManagerCollection.KlippekortRegelManager.GetKlippekortRegelByCrmId(DaekningAfmeldingsRegelCrmId.Value);
                }
                return _daekningKlippekortRegel;
            }
            set { _daekningKlippekortRegel = value; }
        }

        public Guid? SundhedsUdeblivelsesRegelCrmId { get; set; }
        public Guid? ProduktUdeblivelsesRegelCrmId { get; set; }
        public Guid? DaekningUdeblivelsesRegelCrmId { get; set; }
        public Guid? SundhedsAfmeldingsRegelCrmId { get; set; }
        public Guid? ProduktAfmeldingsRegelCrmId { get; set; }
        public Guid? DaekningAfmeldingsRegelCrmId { get; set; }
        public Guid? SundhedsVolumeRegelCrmId { get; set; }
        public Guid? ProduktVolumeRegelCrmId { get; set; }
        public Guid? DaekningVolumeRegelCrmId { get; set; }
        public DateTime? ProfilOphoersDatoLocalTime { get; set; }

        public string ProduktGruppeNavn { get; set; }
        public bool? SkalOphoersdatoOverholdes { get; set; }

        public Guid? FrequencyRegelCrmId { get; set; }

        public FrequencyRule FrequencyRegel
        {
            get
            {
                if (_frequencyRegel == null && FrequencyRegelCrmId.HasValue)
                {
                    // get this property the first time it is referenced

                    _frequencyRegel = ManagerCollection.FrequencyRegelManager.GetFrequencyRegelByCrmId(FrequencyRegelCrmId.Value);
                }

                return _frequencyRegel;
            }
            set { _frequencyRegel = value; }
        }

    }
}
