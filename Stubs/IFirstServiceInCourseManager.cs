﻿using System;
using System.Collections.Generic;

namespace Kata.Stubs
{
    public interface IFirstServiceInCourseManager
    {
        List<FirstServiceInCourseRule> GetFirstServiceForCourseRule(Guid forloebRegelCrmId);
    }
}