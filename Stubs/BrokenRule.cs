﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class BrokenRule
    {
        public RuleType RuleType { get; set; }
        public string WarningText { get; set; }
        public bool Blocking { get; set; }
        public bool DeleteFutureBookingsInCourse { get; set; }
        public bool KosterKlip { get; set; }
        public bool ErAfmeldtForSent { get; set; }
    }
}
