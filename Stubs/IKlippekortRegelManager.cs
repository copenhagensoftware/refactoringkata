﻿using System;

namespace Kata.Stubs
{
    public interface IKlippekortRegelManager
    {
        KlippekortRegel GetKlippekortRegelByCrmId(Guid value);
    }
}