﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public interface IManagerCollection
    {
        IWarningManager WarningManager { get; }
        IAfdelingManager AfdelingManager { get; }
        IAfmeldingsRegelManager AfmeldingsRegelManager { get; }
        IBehandlerManager BehandlerManager { get; }
        IDagsplanManager DagsplanManager { get; }
        IDataManager DataManager { get; }
        IFirstNumberOfBookingsForServiceInCourseManager FirstNumberOfBookingsForServiceInCourseManager { get; }
        IFirstServiceInCourseManager FirstServiceInCourseManager { get; }
        ICourseManager CourseManager { get; }
        ICourseRuleManager CourseRuleManager { get; }
        IAbsenceManager FravaerManager { get; }
        IFravaerstypeManager FravaerstypeManager { get; }
        IHoldManager TeamManager { get; }
        IInaktivManager InaktivManager { get; }
        IKapacitetstidManager KapacitetstidManager { get; }
        IKlientManager KlientManager { get; }
        IKlinikManager KlinikManager { get; }
        IKlippekortManager KlippekortManager { get; }
        IKlippekortManueltTildeltManager KlippekortManueltTildeltManager { get; }
        IKlippekortRegelManager KlippekortRegelManager { get; }
        ICustomerManager KundeManager { get; }
        IRoomManager LokaleManager { get; }
        IMangeTilMangeManager MangeTilMangeManager { get; }
        IProduktGruppeManager ProduktGruppeManager { get; }
        IProduktManager ProduktManager { get; }
        IProfileManager ProfilManager { get; }
        IRegionManager RegionManager { get; }
        IRingelisteManager RingelisteManager { get; }
        ITimeSlotManager TimeManager { get; }
        ITidsafsnitHistorikManager TidsafsnitHistorikManager { get; }
        ITidsafsnitManager TidsafsnitManager { get; }
        ITidshistorikManager TidshistorikManager { get; }
        IUdeblivelsesRegelManager UdeblivelsesRegelManager { get; }
        IUgeplanManager UgeplanManager { get; }
        IVolumeRegelManager VolumeRegelManager { get; }
        IServiceManager ServiceManager { get; }
        INetvaerkManager NetvaerkManager { get; }
        IPositionManager PositionManager { get; }
        IFrequencyRegelManager FrequencyRegelManager { get; }
        IPotentielAfloeserManager PotentielAfloeserManager { get; }
    }
}
