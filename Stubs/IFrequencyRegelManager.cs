﻿using System;

namespace Kata.Stubs
{
    public interface IFrequencyRegelManager
    {
        FrequencyRule GetFrequencyRegelByCrmId(Guid value);
    }
}