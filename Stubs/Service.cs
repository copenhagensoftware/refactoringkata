﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class Service
    {
        public long ServiceId { get; set; }
        public Guid ServiceCrmId { get; set; }
        public string Navn { get; set; }
        public string Beskrivelse { get; set; }
        public bool Administrativ { get; set; }
        public bool Inaktiv { get; set; }
        public string Farvekode { get; set; }
        public int? AntalKlip { get; set; }
        public int? DaekningMaengde { get; set; }
    }
}
