﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class Course
    {
        public long CourseId { get; set; }
        public Guid? ForloebCrmId { get; set; }
        public int numberOfTreatmentsUsed { get; set; }
        public int? AntalAnbefaledeBehandlinger { get; set; }
        public string Beskrivelse { get; set; }
        public string Navn { get; set; }
        /// <summary>
        /// This is actually the CRM GUID
        /// </summary>
        public Guid ProduktId { get; set; }
        /// <summary>
        /// This is actually the CRM GUID
        /// </summary>
        public Guid CourseRuleId { get; set; }
        public string OprettetAf { get; set; }
        public string AfsluttetAf { get; set; }

        public DateTime Oprettet { get; set; }
        public DateTime GyldigTil { get; set; }
        public DateTime? Afsluttet { get; set; }

        public DateTime? OprettetLocalTime { get; set; }
        public DateTime? GyldigTilLocalTime { get; set; }
        public DateTime? AfsluttetLocalTime { get; set; }

        public bool InAktiv { get; set; }

        public long? TherapistId { get; set; }
    }
}
