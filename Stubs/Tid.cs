﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Stubs
{
    public class TimeSlot
    {
        public long TimeSlotId { get; set; }
        public long SequenceNumber { get; set; }
        public long TidsstatusId { get; set; }
        public long? AfmeldingGrund { get; set; }
        public long? TidsafsnitId { get; set; }
        public long ServiceId { get; set; }
        public long? RealiseretYdelseId { get; set; }
        public long? KlientId { get; set; }
        public long LokaleId { get; set; }
        public long TherapistId { get; set; }
        public long? CourseId { get; set; }
        public long? KlippekortId { get; set; }
        public long OrdningstypeId { get; set; }

        public DateTime UtcStart { get; set; } //TODO 2 (DRO): Refaktoriser Utc til LocalTime, da dette er hvad der hentes ud fra databasen
        public DateTime UtcEnd { get; set; }
        public DateTime StartLocalTime { get; set; }
        public DateTime SlutLocalTime { get; set; }

        public string StrUtcStart { get; set; }
        public string StrUtcSlut { get; set; }
        public string KlientPreviaMedlemsnummer { get; set; }
        public bool? IsEmergency { get; set; }

        public Guid? DaekningCrmId { get; set; }
        public Guid? ProduktCrmId { get; set; }
        public Guid? SundhedsordningCrmId { get; set; }
        public Guid? AfmeldingsRegelCrmId { get; set; }
        public Guid? UdeblivelsesRegelGuid { get; set; }
        public bool Team { get; set; }
        public int Holdstoerrelse { get; set; }
        public int NumberOfParticipants { get; set; }
        public bool? LaastForBooking { get; set; }
        public long? AfloestBehandler { get; set; }
        public long? AflystBehandler { get; set; }
        public long? ParentTidId { get; set; }
        public int? TiderIndtilNaestePauseInklDenneTid { get; set; }
    }
}
